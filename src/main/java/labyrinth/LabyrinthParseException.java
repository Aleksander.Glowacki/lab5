package labyrinth;

import java.io.Serial;

public class LabyrinthParseException extends RuntimeException {
	@Serial
	private static final long serialVersionUID = 5524011224197369244L;

	public LabyrinthParseException(String message) {
		super(message);
	}

}
