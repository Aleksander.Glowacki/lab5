package labyrinth;

import java.io.Serial;

public class MovePlayerException extends Exception {
	@Serial
	private static final long serialVersionUID = -6244594256969039528L;

	public MovePlayerException(String message) {
		super(message);
	}
}
